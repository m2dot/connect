module bitbucket.com/m2dot/connect

go 1.15

require (
	github.com/aws/aws-sdk-go v1.37.0
	gopkg.in/ini.v1 v1.62.0
)
