package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"

	"bitbucket.com/m2dot/connect/awsconfig"
	"bitbucket.com/m2dot/connect/cliutil"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
)

func selectProfile() (string, error) {
	profiles, err := awsconfig.GetProfiles()
	if err != nil {
		return "", err
	}

	index, err := cliutil.GetIndex("Select Profile: ", profiles)
	if err != nil {
		return "", err
	}

	return profiles[index], nil
}

func getRunningInstances(sess *session.Session) ([]*ssm.InventoryResultEntity, error) {
	ssmSvc := ssm.New(sess)
	instances, err := ssmSvc.GetInventory(&ssm.GetInventoryInput{
		Filters: []*ssm.InventoryFilter{
			{
				Key: aws.String("AWS:InstanceInformation.InstanceStatus"),
				Values: []*string{
					aws.String("Active"),
				},
			},
		},
	})
	if err != nil {
		return nil, err
	}

	return instances.Entities, nil
}

func selectInstance(sess *session.Session) (*ssm.InventoryResultEntity, error) {
	instances, err := getRunningInstances(sess)
	if err != nil {
		return nil, err
	}

	names := make([]string, 0)
	for _, instance := range instances {
		names = append(names, *instance.Data["AWS:InstanceInformation"].Content[0]["ComputerName"])
	}

	fmt.Printf("\n == Running Instances == \n")
	index, err := cliutil.GetIndex("Select Instance: ", names)
	if err != nil {
		return nil, err
	}

	return instances[index], nil
}

func connectToInstance(instanceId, profile string) error {

	cmd := exec.Command("aws", "ssm", "start-session", "--target", instanceId, "--profile", profile)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)

	go func(sig <-chan os.Signal) {
		for {
			<-sig
			cmd.Process.Signal(os.Interrupt)
		}
	}(sig)

	return cmd.Run()
}

func main() {
	var err error
	flag.Parse()
	input := flag.Arg(0)

	profile := input
	if profile == "" {
		profile, err = selectProfile()
		if err != nil {
			log.Fatal(err)
		}
	}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Profile:           profile,
	}))
	instance, err := selectInstance(sess)
	if err != nil {
		log.Fatal(err)
	}

	if err := connectToInstance(*instance.Id, profile); err != nil {
		log.Fatal(err)
	}
}
