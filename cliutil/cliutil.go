// Package cliutil implements some helper functions for the user to interact on the command line
package cliutil

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

// GetInput prompts the user for input and returns the entered value as string
func GetInput(prompt string) (string, error) {
	fmt.Print(prompt)
	reader := bufio.NewReader(os.Stdin)
	line, _, err := reader.ReadLine()
	return string(line), err
}

// GetIndex displays the given array as list and prompts the user for a choice.
// the choice can be the full name of the item or the displayed index of the item.
func GetIndex(prompt string, options []string) (int, error) {
	for i, option := range options {
		fmt.Printf("[%d] %s\n", i+1, option)
	}

	input, err := GetInput(prompt)
	if err != nil {
		return -1, err
	}

	index, err := strconv.Atoi(input)
	if err == nil {
		if index < 1 || index > len(options) {
			return -1, errors.New("index out of range")
		}
		return index - 1, nil
	}

	for i, option := range options {
		if option == input {
			return i, nil
		}
	}

	return -1, errors.New("could not make any sense of selection")
}
