// Package awsconfig implements functions for manipulating the aws cli config and credential files
package awsconfig

import (
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"gopkg.in/ini.v1"
)

// configPath returns the path to the aws config file
func configPath() (string, error) {
	specialPath := os.Getenv("AWS_CONFIG_FILE")
	if specialPath != "" {
		return specialPath, nil
	}
	currentUser, err := user.Current()
	if err != nil {
		return "", err
	}
	return filepath.Join(currentUser.HomeDir, ".aws", "config"), nil
}

// GetProfiles returns a list of configured profiles
func GetProfiles() ([]string, error) {
	path, err := configPath()
	if err != nil {
		return nil, err
	}
	configFile, err := ini.Load(path)
	if err != nil {
		return nil, err
	}

	profiles := make([]string, 0)
	for _, section := range configFile.Sections()[1:] {
		profiles = append(profiles, strings.TrimPrefix(section.Name(), "profile "))
	}
	return profiles, nil
}
