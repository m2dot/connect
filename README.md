# Connect

This tool is used to connect to ec2 instances from the command line.

## Download

* [Windows](https://bitbucket.org/m2dot/connect/downloads/connect.exe)
* [Mac](https://bitbucket.org/m2dot/connect/downloads/connect)
* [Linux](https://bitbucket.org/m2dot/connect/downloads/connect.x86_64)

## Requirements

Session Manager plugin 

install instructions

https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html

## Usage

If connect is run without parameters, it will ask you for the profile and instance you want to connect to.
You can also specify the **profile** or the **instance id** as paramter on the commandline. 
e.g.
```bash
./connect
./connect ecb
./connect i-0000000000000000
```
